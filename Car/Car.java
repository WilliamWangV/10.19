package Car;

public class Car {
	String brand;
	int seatNum;
	double volume;
	double oilMass;

	public boolean start(double oilMass) {
		if (oilMass > 0) {
			System.out.println("车子启动");
			return true;
		} else {
			System.out.println("车子没油了，走不了了，请加油");
			return false;
		}
	}

	public void run() {
		System.out.println("车子匀速行驶在马路上");
	}

	public void brake() {
		System.out.println("突然刹车，Duangduangduang");
	}

	public boolean speed() {
		System.out.println("车子加速");
		return true;
	}

	public void stop(boolean Brake, double Mass) {
		if (Mass <= 0 || Brake) {
			System.out.println("车子停止");
		} else {
			System.out.println("车子行驶.");
		}
	}
}
