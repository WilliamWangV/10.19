package Maopao;

import java.util.Scanner;

public class Test {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Maopao mp = new Maopao();
		for (int i = 0; i < mp.a.length; i++) {
			System.out.print("请输入第" + (i + 1) + "个数：");
			mp.a[i] = input.nextInt();
		}
		System.out.println("排序前:");
		for (int i = 0; i < mp.a.length; i++) {
			System.out.print(mp.a[i] + " ");
		}
		mp.sort();
		System.out.println();
		System.out.println("排序后:");
		for (int i = 0; i < mp.a.length; i++) {
			System.out.print(mp.a[i] + " ");
		}

	}

}
